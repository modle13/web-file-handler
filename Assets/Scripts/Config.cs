using System.Collections;
using System.Collections.Generic;

public static class Config {
    public static List<string> stats = new List<string>(
        new string[] {
            "clothing.json",
            "energy.json",
            "tool.json",
            "warmth.json"
        }
    );
    public static List<string> events = new List<string>(
        new string[] {
            "acidRain.json",
            "caveIn.json",
            "famine.json",
            "forestFire.json",
            "maintenance.json",
            "monsoon.json",
            "mutantMoths.json",
            "perfectWeather.json",
            "scarceResources.json",
            "snowStorm.json"
        }
    );

    public static List<string> buildings = new List<string>(
        new string[] {
            "chandler",
            "forester",
            "gatherer",
            "hunter",
            "miner",
            "shipwright",
            "smith",
            "tailor",
            "woodcutter"
        }
    );

    public static List<string> research = new List<string>(
        new string[] {
            "automatons"
        }
    );
}
