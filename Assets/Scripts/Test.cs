using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using System;
using UnityEngine.Networking;

public class Test : MonoBehaviour {

    private string[] names;
    private Dictionary<string, string> data = new Dictionary<string, string>();

    public void Start() {
        LoadVillagerNames();
        // LoadEvents();
    }

    public void Update() {
        if (data.ContainsKey("villagerNames")) {
            if (names == null) {
                Debug.Log("villager names are: " + data["villagerNames"]);
                SplitVillagerNames();
            }
        } else {
            Debug.Log("villager names not ready yet");
        }
        // if (eventsJson == "") {
        //     Debug.Log("event json not ready yet");
        // } else {
        //     Debug.Log("event json is: " + eventsJson);
        //     ProcessEventData();
        // }
    }

    private void LoadVillagerNames() {
        string namePath = System.IO.Path.Combine(Application.streamingAssetsPath, "data/villageCenter/villagerNames.txt");
        LoadData(namePath, "villagerNames");
    }

    private void SplitVillagerNames() {
        names = data["villagerNames"].Split(new[] { "\n", "\r", "\r\n" }, System.StringSplitOptions.None);
        Debug.Log("names are " + names.Length);
        foreach (string name in names) {
            Debug.Log(name);
        }
    }

    // private void LoadEvents() {
    //     string dataPath = "data/events";
    //     Dictionary<string, string> availableEntities = fileHandler.GetDataFilePaths(dataPath, Config.events);
    // }

    // private void ProcessEventData() {
    //     foreach (KeyValuePair<string, string> entry in availableEntities) {
    //         Debug.Log("current event to be loaded is " + entry.Value);
    //         string fileContents = "";
    //         fileHandler.LoadData(entry.Value, fileContents);
    //         if (fileContents == "") {
    //             StartCoroutine(WaitForData(fileContents));
    //         }
    //         Debug.Log("jsonString is " + fileContents);
    //         if (fileContents != "") {
    //             Debug.Log("there's some jsons: " + fileContents);
    //         //     EventData data = JsonUtility.FromJson<EventData>(jsonString);
    //         //     events.Add(new Event(data));
    //         }
    //     }
    // }


    private void LoadData(string filePath, string dataName) {
        string message = "LoadStreamingAsset filepath is " + filePath;
        Debug.Log(message);

        if (filePath.Contains("http:") || filePath.Contains("://") || filePath.Contains(":///")) {
            try {
                StartCoroutine(RequestRoutine(filePath, dataName));
            } catch (Exception e) {
                Debug.Log("failed to do unity webrequest stuff");
            }
        } else {
            try {
                data[dataName] = System.IO.File.ReadAllText(filePath);
            } catch (FileNotFoundException e) {
                Debug.Log("problem loading " + filePath);
            }
        }
    }

    private IEnumerator RequestRoutine(string url, string dataName) {
        UnityWebRequest request = UnityWebRequest.Get(url);
        yield return request.SendWebRequest();
        data[dataName] = request.downloadHandler.text;
        Debug.Log("data in request routine is " + data[dataName]);
    }
}
